package com.dreev.area51.client.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class ParticipantCreationRequestDto {

    @JsonProperty("name")
    private final String name;

}
