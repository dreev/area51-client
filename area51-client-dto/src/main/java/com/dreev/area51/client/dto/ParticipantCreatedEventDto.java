package com.dreev.area51.client.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class ParticipantCreatedEventDto {

    @JsonProperty("participant")
    private final ParticipantDto participant;

}
