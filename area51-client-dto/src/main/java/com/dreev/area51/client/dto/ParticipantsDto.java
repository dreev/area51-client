package com.dreev.area51.client.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Builder
@Data
public class ParticipantsDto {

    @JsonProperty("participants")
    private List<ParticipantDto> participants;

}
