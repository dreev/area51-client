package com.dreev.area51.client.http;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.net.URI;

@Getter
@Setter
public class ProblemDto {

    @JsonProperty("type")
    private String type;

    @JsonProperty("title")
    private String title;

    @JsonProperty("status")
    private Integer status;

    @JsonProperty("detail")
    private String detail;

    public URI getType() {
        return null != type
                ? URI.create(type)
                : null;
    }

}
