package com.dreev.area51.client.http;

import com.dreev.area51.client.dto.ParticipantCreationRequestDto;
import com.dreev.area51.client.dto.ParticipantDto;
import com.dreev.area51.client.dto.ParticipantsDto;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class Area51HttpClient {

    private final Area51HttpClientContext httpClientContext;

    public Area51HttpApiContext withAccessToken(String accessToken) {
        return new Area51HttpApiContext(httpClientContext, accessToken);
    }

    public ParticipantsDto findParticipants(String accessToken) {
        return withAccessToken(accessToken)
                .participant()
                .findAll();
    }

    public ParticipantDto getParticipant(String accessToken, String participantId) {
        return withAccessToken(accessToken)
                .participant()
                .get(participantId);
    }

    public ParticipantDto createParticipant(String accessToken, ParticipantCreationRequestDto creationRequest) {
        return withAccessToken(accessToken)
                .participant()
                .create(creationRequest);
    }

}
