package com.dreev.area51.client.http.exception;

import com.dreev.exception.business.BusinessException;

public final class ParticipantNotFoundException extends BusinessException {

    public ParticipantNotFoundException(String message) {
        super(message);
    }

    public ParticipantNotFoundException(Throwable cause) {
        super(cause);
    }

}
