package com.dreev.area51.client.http;

import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableConfigurationProperties(Area51HttpClientProperties.class)
@ComponentScan
public class Area51HttpClientConfiguration {

}
