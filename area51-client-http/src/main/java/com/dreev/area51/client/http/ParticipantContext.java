package com.dreev.area51.client.http;

import com.dreev.area51.client.dto.ParticipantCreationRequestDto;
import com.dreev.area51.client.dto.ParticipantDto;
import com.dreev.area51.client.dto.ParticipantsDto;
import com.dreev.area51.client.http.exception.InvalidParticipantCreationRequestException;
import com.dreev.area51.client.http.exception.ParticipantNotFoundException;
import com.dreev.exception.business.BusinessException;
import com.dreev.exception.technical.WrapperException;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;

import static com.dreev.area51.client.http.Area51HttpApiPaths.*;

@RequiredArgsConstructor
public final class ParticipantContext {

    private final Area51HttpClientContext httpClientContext;
    private final RestTemplate restTemplate;
    private final String accessToken;

    public ParticipantsDto findAll() {
        String resourceUrl = httpClientContext.getProperties()
                .getUrl()
                + PARTICIPANTS_PATH;

        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        headers.setBearerAuth(accessToken);

        HttpEntity<?> entity = new HttpEntity<>(headers);

        try {
            return restTemplate
                    .exchange(resourceUrl, HttpMethod.GET, entity, ParticipantsDto.class)
                    .getBody();
        } catch (HttpStatusCodeException httpError) {
            BusinessException businessException = httpClientContext.getProblemMapper()
                    .map(httpError);

            throw new WrapperException(businessException);
        }
    }

    public ParticipantDto get(String participantId) {
        String resourceUrl = httpClientContext.getProperties()
                .getUrl()
                + PARTICIPANT_PATH.replace(PARTICIPANT_ID_PLACEHOLDER, participantId);

        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        headers.setBearerAuth(accessToken);

        HttpEntity<?> entity = new HttpEntity<>(headers);

        try {
            return restTemplate
                    .exchange(resourceUrl, HttpMethod.GET, entity, ParticipantDto.class)
                    .getBody();
        } catch (HttpStatusCodeException httpError) {
            BusinessException businessException = httpClientContext.getProblemMapper()
                    .map(httpError);

            if (businessException instanceof ParticipantNotFoundException) {
                throw businessException;
            }

            throw new WrapperException(businessException);
        }
    }

    public ParticipantDto create(ParticipantCreationRequestDto creationRequest) {
        String resourceUrl = httpClientContext.getProperties()
                .getUrl()
                + PARTICIPANTS_PATH;

        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        headers.setBearerAuth(accessToken);

        HttpEntity<?> entity = new HttpEntity<>(creationRequest, headers);

        try {
            return restTemplate
                    .exchange(resourceUrl, HttpMethod.POST, entity, ParticipantDto.class)
                    .getBody();
        } catch (HttpStatusCodeException httpError) {
            BusinessException businessException = httpClientContext.getProblemMapper()
                    .map(httpError);

            if (businessException instanceof InvalidParticipantCreationRequestException) {
                throw businessException;
            }

            throw new WrapperException(businessException);
        }
    }

}
