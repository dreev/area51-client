package com.dreev.area51.client.http;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties("dreev.area51-http-api")
@Data
public class Area51HttpClientProperties {

    private String url;

}
