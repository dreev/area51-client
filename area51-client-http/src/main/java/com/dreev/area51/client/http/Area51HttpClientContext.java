package com.dreev.area51.client.http;

import lombok.Data;
import org.springframework.stereotype.Component;

@Component
@Data
public class Area51HttpClientContext {

    private final Area51HttpApiProblemMapper problemMapper;
    private final Area51HttpClientProperties properties;

}
