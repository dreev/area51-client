package com.dreev.area51.client.http;

import com.dreev.area51.client.http.exception.InvalidParticipantCreationRequestException;
import com.dreev.area51.client.http.exception.ParticipantNotFoundException;
import com.dreev.exception.business.BusinessException;
import com.dreev.exception.technical.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpStatusCodeException;

import java.util.Optional;

import static com.dreev.area51.client.http.Area51HttpApiErrorType.INVALID_PARTICIPANT_CREATION_REQUEST;
import static com.dreev.area51.client.http.Area51HttpApiErrorType.PARTICIPANT_NOT_FOUND;

@Component
@RequiredArgsConstructor
public class Area51HttpApiProblemMapper {

    private final ObjectMapper objectMapper;

    BusinessException map(HttpStatusCodeException httpError) {
        BusinessException businessException = null;

        try {
            ProblemDto problem = objectMapper.readValue(httpError.getResponseBodyAsByteArray(), ProblemDto.class);

            if (PARTICIPANT_NOT_FOUND.getType().equals(problem.getType())) {
                businessException = new ParticipantNotFoundException(httpError);
            }

            if (INVALID_PARTICIPANT_CREATION_REQUEST.getType().equals(problem.getType())) {
                businessException = new InvalidParticipantCreationRequestException(problem.getDetail(), httpError);
            }

            if (HttpStatus.UNAUTHORIZED.getReasonPhrase().equals(problem.getTitle())) {
                throw new Unauthorized(httpError);
            }

            if (HttpStatus.FORBIDDEN.getReasonPhrase().equals(problem.getTitle())) {
                throw new Forbidden(httpError);
            }

            if (HttpStatus.METHOD_NOT_ALLOWED.getReasonPhrase().equals(problem.getTitle())) {
                throw new MethodNotAllowed(httpError);
            }

            if (HttpStatus.NOT_ACCEPTABLE.getReasonPhrase().equals(problem.getTitle())) {
                throw new NotAcceptable(httpError);
            }

            if (HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase().equals(problem.getTitle())) {
                throw new InternalServerError(httpError);
            }

            if (HttpStatus.SERVICE_UNAVAILABLE.getReasonPhrase().equals(problem.getTitle())) {
                throw new ServiceUnavailable(httpError);
            }
        } catch (TechnicalException e) {
            throw e;
        } catch (Exception e) {
            throw new WrapperException(e);
        }

        return Optional.ofNullable(businessException)
                .orElseThrow(() -> new WrapperException(httpError));
    }

}
