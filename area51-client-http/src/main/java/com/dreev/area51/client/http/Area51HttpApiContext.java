package com.dreev.area51.client.http;

import lombok.RequiredArgsConstructor;
import org.springframework.web.client.RestTemplate;

@RequiredArgsConstructor
public final class Area51HttpApiContext {

    private final Area51HttpClientContext httpClientContext;
    private final String accessToken;

    public ParticipantContext participant() {
        return new ParticipantContext(httpClientContext, new RestTemplate(), accessToken);
    }

}
