package com.dreev.area51.client.http.exception;

import com.dreev.exception.business.BusinessException;

public class InvalidParticipantCreationRequestException extends BusinessException {

    public InvalidParticipantCreationRequestException(String message, Throwable cause) {
        super(message, cause);
    }

}
