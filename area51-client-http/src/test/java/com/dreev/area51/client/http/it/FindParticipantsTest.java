package com.dreev.area51.client.http.it;

import com.dreev.area51.client.dto.ParticipantDto;
import com.dreev.area51.client.dto.ParticipantsDto;
import com.dreev.area51.client.http.Area51HttpClient;
import com.dreev.area51.client.http.test.util.TestApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.contract.wiremock.AutoConfigureWireMock;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = TestApplication.class)
@AutoConfigureWireMock(port = 0)
public class FindParticipantsTest {

    @Autowired
    private Area51HttpClient area51HttpClient;

    @Test
    public void when_find_then_ok() {
        ParticipantsDto actual = area51HttpClient.findParticipants("abc");

        List<ParticipantDto> participants = actual.getParticipants();
        assertThat(participants).isNotNull();
        assertThat(participants).hasSize(1);

        ParticipantDto participant = participants.get(0);
        assertThat(participant.getId()).isEqualTo("2829cfaa-e701-4ab6-bfe1-79f783ef73ca");
        assertThat(participant.getName()).isEqualTo("myNewParticipant");
    }

}
