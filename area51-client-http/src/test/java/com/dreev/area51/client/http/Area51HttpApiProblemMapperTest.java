package com.dreev.area51.client.http;

import com.dreev.area51.client.http.exception.InvalidParticipantCreationRequestException;
import com.dreev.area51.client.http.exception.ParticipantNotFoundException;
import com.dreev.exception.business.BusinessException;
import com.dreev.exception.technical.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpStatusCodeException;

import java.io.IOException;

import static com.dreev.area51.client.http.Area51HttpApiErrorType.INVALID_PARTICIPANT_CREATION_REQUEST;
import static com.dreev.area51.client.http.Area51HttpApiErrorType.PARTICIPANT_NOT_FOUND;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class Area51HttpApiProblemMapperTest {

    @Mock
    private HttpStatusCodeException httpError;

    @Mock
    private ProblemDto problem;

    @Mock
    private ObjectMapper objectMapper;

    @InjectMocks
    private Area51HttpApiProblemMapper problemMapper;

    @Test
    public void given_error_was_participant_not_found_when_map_then_ok() throws IOException {
        when(objectMapper.readValue(eq((byte[]) null), eq(ProblemDto.class)))
                .thenReturn(problem);

        when(problem.getType())
                .thenReturn(PARTICIPANT_NOT_FOUND.getType());

        BusinessException exception = problemMapper.map(httpError);
        assertThat(exception).isInstanceOf(ParticipantNotFoundException.class);
    }

    @Test
    public void given_error_was_invalid_participant_creation_request_when_map_then_ok() throws IOException {
        when(objectMapper.readValue(eq((byte[]) null), eq(ProblemDto.class)))
                .thenReturn(problem);

        when(problem.getType())
                .thenReturn(INVALID_PARTICIPANT_CREATION_REQUEST.getType());

        BusinessException exception = problemMapper.map(httpError);
        assertThat(exception).isInstanceOf(InvalidParticipantCreationRequestException.class);
    }

    @Test(expected = Unauthorized.class)
    public void given_error_was_unauthorized_when_map_then_error() throws IOException {
        when(objectMapper.readValue(eq((byte[]) null), eq(ProblemDto.class)))
                .thenReturn(problem);

        when(problem.getTitle())
                .thenReturn(HttpStatus.UNAUTHORIZED.getReasonPhrase());

        problemMapper.map(httpError);
    }

    @Test(expected = Forbidden.class)
    public void given_error_was_forbidden_when_map_then_error() throws IOException {
        when(objectMapper.readValue(eq((byte[]) null), eq(ProblemDto.class)))
                .thenReturn(problem);

        when(problem.getTitle())
                .thenReturn(HttpStatus.FORBIDDEN.getReasonPhrase());

        problemMapper.map(httpError);
    }

    @Test(expected = MethodNotAllowed.class)
    public void given_error_was_method_not_allowed_when_map_then_error() throws IOException {
        when(objectMapper.readValue(eq((byte[]) null), eq(ProblemDto.class)))
                .thenReturn(problem);

        when(problem.getTitle())
                .thenReturn(HttpStatus.METHOD_NOT_ALLOWED.getReasonPhrase());

        problemMapper.map(httpError);
    }

    @Test(expected = NotAcceptable.class)
    public void given_error_was_not_acceptable_when_map_then_error() throws IOException {
        when(objectMapper.readValue(eq((byte[]) null), eq(ProblemDto.class)))
                .thenReturn(problem);

        when(problem.getTitle())
                .thenReturn(HttpStatus.NOT_ACCEPTABLE.getReasonPhrase());

        problemMapper.map(httpError);
    }

    @Test(expected = InternalServerError.class)
    public void given_error_was_internal_server_error_when_map_then_error() throws IOException {
        when(objectMapper.readValue(eq((byte[]) null), eq(ProblemDto.class)))
                .thenReturn(problem);

        when(problem.getTitle())
                .thenReturn(HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase());

        problemMapper.map(httpError);
    }

    @Test(expected = ServiceUnavailable.class)
    public void given_error_was_service_unavailable_when_map_then_error() throws IOException {
        when(objectMapper.readValue(eq((byte[]) null), eq(ProblemDto.class)))
                .thenReturn(problem);

        when(problem.getTitle())
                .thenReturn(HttpStatus.SERVICE_UNAVAILABLE.getReasonPhrase());

        problemMapper.map(httpError);
    }

    @Test(expected = TechnicalException.class)
    public void given_unknown_server_error_when_map_then_error() throws IOException {
        when(objectMapper.readValue(eq((byte[]) null), eq(ProblemDto.class)))
                .thenReturn(problem);

        problemMapper.map(httpError);
    }

    @Test(expected = TechnicalException.class)
    public void given_error_during_deserialization_when_map_then_error() throws IOException {
        when(objectMapper.readValue(eq((byte[]) null), eq(ProblemDto.class)))
                .thenThrow(new IOException());

        problemMapper.map(httpError);
    }

}
