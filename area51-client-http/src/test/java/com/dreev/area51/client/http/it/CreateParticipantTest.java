package com.dreev.area51.client.http.it;

import com.dreev.area51.client.dto.ParticipantCreationRequestDto;
import com.dreev.area51.client.dto.ParticipantDto;
import com.dreev.area51.client.http.Area51HttpClient;
import com.dreev.area51.client.http.test.util.TestApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.contract.wiremock.AutoConfigureWireMock;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = TestApplication.class)
@AutoConfigureWireMock(port = 0)
public class CreateParticipantTest {

    @Autowired
    private Area51HttpClient area51HttpClient;

    @Test
    public void given_valid_request_when_create_then_ok() {
        ParticipantCreationRequestDto participantCreationRequest = ParticipantCreationRequestDto.builder()
                .name("myNewParticipant")
                .build();

        ParticipantDto participant = area51HttpClient.createParticipant("abc", participantCreationRequest);

        assertThat(participant.getId()).isEqualTo("2829cfaa-e701-4ab6-bfe1-79f783ef73ca");
        assertThat(participant.getName()).isEqualTo("myNewParticipant");
    }

}
