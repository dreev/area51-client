package com.dreev.area51.client.http;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class Area51HttpClientTest {

    @Mock
    private Area51HttpClientContext httpClientContext;

    @InjectMocks
    private Area51HttpClient area51HttpClient;

    @Test
    public void when_withAccessToken_then_ok() {
        Area51HttpApiContext fleetHttpApiContext = area51HttpClient.withAccessToken("abc");
        assertThat(fleetHttpApiContext).isNotNull();
    }

}
