package com.dreev.area51.client.http.it;

import com.dreev.area51.client.dto.ParticipantDto;
import com.dreev.area51.client.http.Area51HttpClient;
import com.dreev.area51.client.http.exception.ParticipantNotFoundException;
import com.dreev.area51.client.http.test.util.TestApplication;
import com.dreev.exception.technical.Forbidden;
import com.dreev.exception.technical.InternalServerError;
import com.dreev.exception.technical.TechnicalException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.contract.wiremock.AutoConfigureWireMock;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.fail;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = TestApplication.class)
@AutoConfigureWireMock(port = 0)
public class GetParticipantTest {

    @Autowired
    private Area51HttpClient area51HttpClient;

    @Test
    public void given_valid_request_when_get_then_ok() {
        ParticipantDto participant = area51HttpClient.getParticipant("abc", "2829cfaa-e701-4ab6-bfe1-79f783ef73ca");
        assertThat(participant.getId()).isEqualTo("2829cfaa-e701-4ab6-bfe1-79f783ef73ca");
        assertThat(participant.getName()).isEqualTo("myParticipant");
    }

    @Test(expected = Forbidden.class)
    public void given_forbidden_request_when_get_then_error() {
        area51HttpClient.getParticipant("bca", "2829cfaa-e701-4ab6-bfe1-79f783ef73ca");
        fail("expected Technical exception");
    }

    @Test(expected = ParticipantNotFoundException.class)
    public void given_participant_does_not_exist_when_get_then_error() {
        area51HttpClient.getParticipant("abc", "912ebcfa-0168-41e7-be5c-d64f36dfc36c");
    }

    @Test(expected = InternalServerError.class)
    public void given_internal_server_error_when_get_then_error() {
        area51HttpClient.getParticipant("abc", "42cbf12a-d8fa-422d-865c-3bda0076111a");
    }

    @Test(expected = TechnicalException.class)
    public void given_error_but_not_parsable_when_get_then_error() {
        area51HttpClient.getParticipant("abc", "69adfb5b-e86d-4377-a7e4-f0e592f3e40b");
    }

}
