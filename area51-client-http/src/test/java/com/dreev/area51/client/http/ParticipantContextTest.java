package com.dreev.area51.client.http;

import com.dreev.area51.client.dto.ParticipantCreationRequestDto;
import com.dreev.area51.client.dto.ParticipantDto;
import com.dreev.area51.client.dto.ParticipantsDto;
import com.dreev.area51.client.http.exception.InvalidParticipantCreationRequestException;
import com.dreev.area51.client.http.exception.ParticipantNotFoundException;
import com.dreev.exception.business.BusinessException;
import com.dreev.exception.technical.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;

import java.util.UUID;

import static com.dreev.area51.client.http.Area51HttpApiPaths.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class ParticipantContextTest {

    private static final String PARTICIPANT_ID = UUID.randomUUID().toString();
    private static final String _PARTICIPANT_PATH = PARTICIPANT_PATH.replace(PARTICIPANT_ID_PLACEHOLDER, PARTICIPANT_ID);

    @Mock
    private ParticipantDto participant;

    @Mock
    private ParticipantsDto participants;

    @Mock
    private ParticipantCreationRequestDto participantCreationRequest;

    @Mock
    private Area51HttpClientContext httpClientContext;

    @Mock
    private Area51HttpClientProperties httpClientProperties;

    @Mock
    private RestTemplate restTemplate;

    @Mock
    private Area51HttpApiProblemMapper problemMapper;

    private ParticipantContext participantContext;

    private ArgumentCaptor<HttpEntity> httpEntityCaptor = ArgumentCaptor.forClass(HttpEntity.class);

    @Before
    public void before() {
        participantContext = new ParticipantContext(httpClientContext, restTemplate, "abc");

        when(httpClientContext.getProperties())
                .thenReturn(httpClientProperties);

        when(httpClientProperties.getUrl())
                .thenReturn("");

        when(httpClientContext.getProblemMapper())
                .thenReturn(problemMapper);

        when(participant.getId())
                .thenReturn(PARTICIPANT_ID);
    }

    @Test
    public void when_get_then_headers_are_valid() {
        when(restTemplate.exchange(eq(_PARTICIPANT_PATH), eq(HttpMethod.GET), any(HttpEntity.class), eq(ParticipantDto.class)))
                .thenReturn(ResponseEntity.ok(participant));

        participantContext.get(PARTICIPANT_ID);

        verify(restTemplate, times(1))
                .exchange(eq(_PARTICIPANT_PATH), eq(HttpMethod.GET), httpEntityCaptor.capture(), eq(ParticipantDto.class));

        HttpEntity<?> httpEntity = httpEntityCaptor.getValue();
        assertThat(httpEntity.getHeaders().getAccept()).containsExactlyInAnyOrder(MediaType.APPLICATION_JSON);
        assertThat(httpEntity.getHeaders().get("Authorization")).containsExactlyInAnyOrder("Bearer abc");
    }

    @Test
    public void when_get_then_response_is_ok() {
        when(restTemplate.exchange(eq(_PARTICIPANT_PATH), eq(HttpMethod.GET), any(HttpEntity.class), eq(ParticipantDto.class)))
                .thenReturn(ResponseEntity.ok(participant));

        ParticipantDto participant = participantContext.get(PARTICIPANT_ID);
        assertThat(participant.getId()).isEqualTo(PARTICIPANT_ID);
    }

    @Test(expected = ParticipantNotFoundException.class)
    public void given_participant_does_not_exist_when_get_then_error() {
        HttpStatusCodeException exception = Mockito.mock(HttpStatusCodeException.class);

        when(restTemplate.exchange(eq(_PARTICIPANT_PATH), eq(HttpMethod.GET), any(HttpEntity.class), eq(ParticipantDto.class)))
                .thenThrow(exception);

        when(problemMapper.map(eq(exception)))
                .thenReturn(new ParticipantNotFoundException(PARTICIPANT_ID));

        participantContext.get(PARTICIPANT_ID);
    }

    @Test(expected = TechnicalException.class)
    public void given_unmapped_business_error_when_get_then_error() {
        BusinessException businessException = Mockito.mock(BusinessException.class);

        HttpStatusCodeException exception = Mockito.mock(HttpStatusCodeException.class);
        when(restTemplate.exchange(eq(_PARTICIPANT_PATH), eq(HttpMethod.GET), any(HttpEntity.class), eq(ParticipantDto.class)))
                .thenThrow(exception);

        when(problemMapper.map(eq(exception)))
                .thenReturn(businessException);

        participantContext.get(PARTICIPANT_ID);
    }

    @Test(expected = Unauthorized.class)
    public void given_unauthorized_when_get_then_error() {
        HttpStatusCodeException exception = Mockito.mock(HttpStatusCodeException.class);
        when(restTemplate.exchange(eq(_PARTICIPANT_PATH), eq(HttpMethod.GET), any(HttpEntity.class), eq(ParticipantDto.class)))
                .thenThrow(exception);

        when(problemMapper.map(eq(exception)))
                .thenThrow(new Unauthorized(new RuntimeException()));

        participantContext.get(PARTICIPANT_ID);
    }

    @Test(expected = Forbidden.class)
    public void given_forbidden_when_get_then_error() {
        HttpStatusCodeException exception = Mockito.mock(HttpStatusCodeException.class);
        when(restTemplate.exchange(eq(_PARTICIPANT_PATH), eq(HttpMethod.GET), any(HttpEntity.class), eq(ParticipantDto.class)))
                .thenThrow(exception);

        when(problemMapper.map(eq(exception)))
                .thenThrow(new Forbidden(new RuntimeException()));

        participantContext.get(PARTICIPANT_ID);
    }

    @Test(expected = InternalServerError.class)
    public void given_internal_server_error_when_get_then_error() {
        HttpStatusCodeException exception = Mockito.mock(HttpStatusCodeException.class);
        when(restTemplate.exchange(eq(_PARTICIPANT_PATH), eq(HttpMethod.GET), any(HttpEntity.class), eq(ParticipantDto.class)))
                .thenThrow(exception);

        when(problemMapper.map(eq(exception)))
                .thenThrow(new InternalServerError(new RuntimeException()));

        participantContext.get(PARTICIPANT_ID);
    }

    @Test(expected = ServiceUnavailable.class)
    public void given_service_unavailable_when_get_then_error() {
        HttpStatusCodeException exception = Mockito.mock(HttpStatusCodeException.class);
        when(restTemplate.exchange(eq(_PARTICIPANT_PATH), eq(HttpMethod.GET), any(HttpEntity.class), eq(ParticipantDto.class)))
                .thenThrow(exception);

        when(problemMapper.map(eq(exception)))
                .thenThrow(new ServiceUnavailable(new RuntimeException()));

        participantContext.get(PARTICIPANT_ID);
    }

    @Test
    public void when_create_then_headers_are_valid() {
        when(restTemplate.exchange(eq(PARTICIPANTS_PATH), eq(HttpMethod.POST), any(HttpEntity.class), eq(ParticipantDto.class)))
                .thenReturn(ResponseEntity.ok(participant));

        participantContext.create(participantCreationRequest);

        verify(restTemplate, times(1))
                .exchange(eq(PARTICIPANTS_PATH), eq(HttpMethod.POST), httpEntityCaptor.capture(), eq(ParticipantDto.class));

        HttpEntity<?> httpEntity = httpEntityCaptor.getValue();
        assertThat(httpEntity.getHeaders().getAccept()).containsExactlyInAnyOrder(MediaType.APPLICATION_JSON);
        assertThat(httpEntity.getHeaders().get("Authorization")).containsExactlyInAnyOrder("Bearer abc");
    }

    @Test
    public void when_create_then_response_is_ok() {
        when(restTemplate.exchange(eq(PARTICIPANTS_PATH), eq(HttpMethod.POST), any(HttpEntity.class), eq(ParticipantDto.class)))
                .thenReturn(ResponseEntity.ok(participant));

        ParticipantDto participant = participantContext.create(participantCreationRequest);
        assertThat(participant.getId()).isEqualTo(PARTICIPANT_ID);
    }

    @Test(expected = InvalidParticipantCreationRequestException.class)
    public void given_invalid_participant_creation_request_when_create_then_error() {
        HttpStatusCodeException exception = Mockito.mock(HttpStatusCodeException.class);
        when(restTemplate.exchange(eq(PARTICIPANTS_PATH), eq(HttpMethod.POST), any(HttpEntity.class), eq(ParticipantDto.class)))
                .thenThrow(exception);

        when(problemMapper.map(eq(exception)))
                .thenReturn(new InvalidParticipantCreationRequestException("some reason", new RuntimeException()));

        participantContext.create(participantCreationRequest);
    }

    @Test(expected = TechnicalException.class)
    public void given_unmapped_business_error_when_create_then_error() {
        BusinessException businessException = Mockito.mock(BusinessException.class);

        HttpStatusCodeException exception = Mockito.mock(HttpStatusCodeException.class);
        when(restTemplate.exchange(eq(PARTICIPANTS_PATH), eq(HttpMethod.POST), any(HttpEntity.class), eq(ParticipantDto.class)))
                .thenThrow(exception);

        when(problemMapper.map(eq(exception)))
                .thenReturn(businessException);

        participantContext.create(participantCreationRequest);
    }

    @Test(expected = Unauthorized.class)
    public void given_unauthorized_when_create_then_error() {
        HttpStatusCodeException exception = Mockito.mock(HttpStatusCodeException.class);
        when(restTemplate.exchange(eq(PARTICIPANTS_PATH), eq(HttpMethod.POST), any(HttpEntity.class), eq(ParticipantDto.class)))
                .thenThrow(exception);

        when(problemMapper.map(eq(exception)))
                .thenThrow(new Unauthorized(new RuntimeException()));

        participantContext.create(participantCreationRequest);
    }

    @Test(expected = Forbidden.class)
    public void given_forbidden_when_create_then_error() {
        HttpStatusCodeException exception = Mockito.mock(HttpStatusCodeException.class);
        when(restTemplate.exchange(eq(PARTICIPANTS_PATH), eq(HttpMethod.POST), any(HttpEntity.class), eq(ParticipantDto.class)))
                .thenThrow(exception);

        when(problemMapper.map(eq(exception)))
                .thenThrow(new Forbidden(new RuntimeException()));

        participantContext.create(participantCreationRequest);
    }

    @Test(expected = InternalServerError.class)
    public void given_internal_server_error_when_create_then_error() {
        HttpStatusCodeException exception = Mockito.mock(HttpStatusCodeException.class);
        when(restTemplate.exchange(eq(PARTICIPANTS_PATH), eq(HttpMethod.POST), any(HttpEntity.class), eq(ParticipantDto.class)))
                .thenThrow(exception);

        when(problemMapper.map(eq(exception)))
                .thenThrow(new InternalServerError(new RuntimeException()));

        participantContext.create(participantCreationRequest);
    }

    @Test(expected = ServiceUnavailable.class)
    public void given_service_unavailable_when_create_then_error() {
        HttpStatusCodeException exception = Mockito.mock(HttpStatusCodeException.class);
        when(restTemplate.exchange(eq(PARTICIPANTS_PATH), eq(HttpMethod.POST), any(HttpEntity.class), eq(ParticipantDto.class)))
                .thenThrow(exception);

        when(problemMapper.map(eq(exception)))
                .thenThrow(new ServiceUnavailable(new RuntimeException()));

        participantContext.create(participantCreationRequest);
    }

    @Test
    public void when_findAll_then_headers_are_valid() {
        when(restTemplate.exchange(eq(PARTICIPANTS_PATH), eq(HttpMethod.GET), any(HttpEntity.class), eq(ParticipantsDto.class)))
                .thenReturn(ResponseEntity.ok(participants));

        participantContext.findAll();

        verify(restTemplate, times(1))
                .exchange(eq(PARTICIPANTS_PATH), eq(HttpMethod.GET), httpEntityCaptor.capture(), eq(ParticipantsDto.class));

        HttpEntity<?> httpEntity = httpEntityCaptor.getValue();
        assertThat(httpEntity.getHeaders().getAccept()).containsExactlyInAnyOrder(MediaType.APPLICATION_JSON);
        assertThat(httpEntity.getHeaders().get("Authorization")).containsExactlyInAnyOrder("Bearer abc");
    }

    @Test
    public void when_findAll_then_response_is_ok() {
        when(restTemplate.exchange(eq(PARTICIPANTS_PATH), eq(HttpMethod.GET), any(HttpEntity.class), eq(ParticipantsDto.class)))
                .thenReturn(ResponseEntity.ok(participants));

        ParticipantsDto actual = participantContext.findAll();
        assertThat(actual).isEqualTo(participants);
    }

    @Test(expected = TechnicalException.class)
    public void given_unmapped_business_error_when_findAll_then_error() {
        BusinessException businessException = Mockito.mock(BusinessException.class);

        HttpStatusCodeException exception = Mockito.mock(HttpStatusCodeException.class);
        when(restTemplate.exchange(eq(PARTICIPANTS_PATH), eq(HttpMethod.GET), any(HttpEntity.class), eq(ParticipantsDto.class)))
                .thenThrow(exception);

        when(problemMapper.map(eq(exception)))
                .thenReturn(businessException);

        participantContext.findAll();
    }

}
