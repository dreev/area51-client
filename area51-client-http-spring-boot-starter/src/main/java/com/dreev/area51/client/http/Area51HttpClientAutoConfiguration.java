package com.dreev.area51.client.http;

import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConditionalOnClass(Area51HttpClient.class)
@ComponentScan
public class Area51HttpClientAutoConfiguration {

}
