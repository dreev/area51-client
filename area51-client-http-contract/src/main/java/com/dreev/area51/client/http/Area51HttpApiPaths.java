package com.dreev.area51.client.http;

public final class Area51HttpApiPaths {

    public static final String PARTICIPANT_ID = "participantId";
    public static final String PARTICIPANT_ID_PLACEHOLDER = '{' + PARTICIPANT_ID + '}';
    public static final String PARTICIPANT_ID_PATH = '/' + PARTICIPANT_ID_PLACEHOLDER;
    public static final String PARTICIPANTS_PATH = '/' + "participants";
    public static final String PARTICIPANT_PATH = PARTICIPANTS_PATH + PARTICIPANT_ID_PATH;

    Area51HttpApiPaths() {

    }

}
