package com.dreev.area51.client.http;

import lombok.Getter;

import java.net.URI;

@Getter
public enum Area51HttpApiErrorType {

    PARTICIPANT_NOT_FOUND(
            "http://api.nuvve.com/problem/participant-not-found",
            "Participant Not Found",
            404,
            "No participant found for the given id"
    ),

    INVALID_PARTICIPANT_CREATION_REQUEST(
            "http://api.nuvve.com/problem/invalid-participant-creation-request",
            "Invalid participant creation request",
            400,
            "Participant creation request is invalid"
    ),

    ;

    private final URI type;
    private final String title;
    private final int httpStatusCode;
    private final String detail;

    Area51HttpApiErrorType(String type, String title, int httpStatusCode, String detail) {
        this.type = URI.create(type);
        this.title = title;
        this.httpStatusCode = httpStatusCode;
        this.detail = detail;
    }

}
