package com.dreev.area51.client.http;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class Area51HttpApiErrorTypeTest {

    @Test
    public void given_PARTICIPANT_NOT_FOUND_when_new_then_ok() {
        Area51HttpApiErrorType errorType = Area51HttpApiErrorType.PARTICIPANT_NOT_FOUND;
        assertThat(errorType).isNotNull();
    }

    @Test
    public void given_INVALID_PARTICIPANT_CREATION_REQUEST_when_new_then_ok() {
        Area51HttpApiErrorType errorType = Area51HttpApiErrorType.INVALID_PARTICIPANT_CREATION_REQUEST;
        assertThat(errorType).isNotNull();
    }

}
